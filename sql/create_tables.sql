
create table roles ( 			   	    /* This table holds the LOST roles */
	role_pk		serial primary key, /*Counts each role*/
	role		varchar(32) UNIQUE not null /*Name of the role*/
);

Insert Into roles (role) VALUES ('Logistics Officer');
INSERT INTO roles (role) VALUES ('Facilities Officer');
create table users (				    /*This table will hold user data*/
	user_pk		serial primary key,         /*Give each user a unique identifier */
	username	varchar(16) UNIQUE not null,/*Unique usernames no longer than 16 characters, required for each user */
	password	varchar(16) not null, 	    /*passwords no longer than 16 characeters, required for each user */
	role		varchar(32) REFERENCES roles(role) not null,/*Each user has a role*/
	active		varchar(5) not null /*Whether or not the current user is allowed to log in"*/
);

create table facilities (			/*This table holds the list of LOST facilities*/
	fcode		varchar(6) UNIQUE not null, /*Facility code ID*/
	common_name	varchar(32) UNIQUE not null /*Facility common name*/
);

create table assets (                        /*This table holds records of LOST assets */
	asset_pk	serial primary key,  /* Unique identifier for each asset */
	asset_tag	varchar(16) UNIQUE not null,/*Each asset has a unique tag*/
	description	text,		     /*A description of the asset*/
	facility	varchar(6) REFERENCES facilities(fcode) not null, /*Assets belong to a facility*/
	aquired		timestamp, --UTC
	disposed	timestamp --Once an asset has been disposed, the date when it occured is presented here
);

create table transit_req ( /*This table holds asset transit requests*/
	asset_tag	varchar(16) not null,	
	source		varchar(6)  not null,
	destination	varchar(6)  not null,
	request_by	varchar(16)  not null,
	request_dt	timestamp not null,
	approve_by	varchar(16),
	approve_dt	timestamp,
	load_dt		timestamp,
	unload_dt 	timestamp,
	status		text	/*Indicates whether an asset has been marked for approval. Values are either APPROVED or null*/
);
