#!/bin/bash

#psql $1 -c /set v1 $2
#psql $1 -f export_data.sql
user=/users.csv
facilities=/facilities.csv
assets=/assets.csv
transfers=/transfers.csv
if [ ! -d $2 ]; then
	mkdir $2
fi

cd $2
#Import user table
sed -i -e 's/NULL//g' $2$user
psql $1 -c "\copy users(username,password,role,active) FROM '"$2$user"' WITH DELIMITER ',' CSV HEADER"
#Import facilities table
sed -i -e 's/NULL//g' $2$facilities
psql $1 -c "\copy facilities(fcode,common_name) FROM '"$2$facilities"' WITH DELIMITER ',' CSV HEADER"
#Import assets table
sed -i -e 's/NULL//g' $2$assets
psql $1 -c "\copy assets(asset_tag, description, facility, aquired, disposed) FROM '"$2$assets"' WITH DELIMITER ',' CSV HEADER"
#Import transfers table
sed -i -e 's/NULL//g' $2$transfers
psql $1 -c "\copy transit_req(asset_tag,request_by,request_dt,approve_by,approve_dt, source, destination, load_dt, unload_dt) FROM '"$2$transfers"' WITH DELIMITER ',' CSV HEADER"
