from flask import Flask, render_template, request, session, redirect, url_for
import psycopg2
from config import dbname, dbhost, dbport
import json

SECRET_KEY = 'FKc4c2111cA23fEEeee'

app = Flask(__name__)
app.config.from_object(__name__)
conn = psycopg2.connect(dbname=dbname, host=dbhost, port=dbport)
conn.autocommit = True
cur = conn.cursor()
@app.route('/', methods=['GET'])
@app.route('/login', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    if request.method =='POST':
        userInfo = request.form.get('username')
        passInfo = request.form.get('password')
        cur.execute('SELECT username, password FROM users WHERE (username, password, active) = (%s, %s, %s)', (userInfo, passInfo, "True"))
        checkUserInput = cur.fetchone()
        if checkUserInput == None:
            return render_template('index.html', status="Invalid LOST user credentials")
        else:
            session['user'] = userInfo
            cur.execute('SELECT role FROM users WHERE username = %s', (userInfo,))
            check_role = cur.fetchone()
            session['role'] = check_role[0]
            return redirect(url_for('dashboard'))


@app.route('/activate_user', methods=['GET', 'POST'])
def create_user():
    if request.method == 'POST':
        userInfo = request.form.get('username')
        passInfo = request.form.get('password')
        roleInfo = request.form.get('role')
        #if userInfo == '' or passInfo == '':
        #    return render_template('create_user.html', error="Error: You must enter both a username and a password")
        #elif len(userInfo) > 16 or len(passInfo) > 16:
        #    return render_template('create_user.html', error="Error: Username and Password have a max length of 16")
        #elif " " in userInfo or " " in passInfo:
        #    return render_template('create_user.html', error="Error: No spaces allowed in username or password")
        #elif roleInfo == '':
        #    return render_template('create_user.html', error="Error: Must select a role")
        #else:#Check if username is already taken
        cur.execute('SELECT username FROM users WHERE username = %s', (userInfo,))
        checkUser = cur.fetchone()
        if checkUser == None:#if not taken, add to db
            cur.execute('INSERT INTO users (username, password, role, active) VALUES (%s, %s, %s, %s)', (userInfo, passInfo, roleInfo, "True"))
            return redirect(url_for('index'))
                #return render_template('create_user.html', error="User has been added to LOST")
            #else:#If taken 
            #    return render_template('create_user.html', error="This name is taken")


@app.route('/revoke_user', methods = ['POST'])
def revoke_user():
    if request.method == 'POST':
        userInfo = request.form.get('username')
        cur.execute('UPDATE users SET active = %s WHERE username = %s', ("False", userInfo))
        return redirect(url_for('index'))

@app.route('/add_facility', methods = ['GET','POST'])
def add_facility():
    cur.execute('SELECT common_name, fcode FROM facilities')
    items = cur.fetchall()
    if request.method == 'GET':
        return render_template('add_facility.html', items = items)
    if request.method == 'POST':
        common_name = request.form.get('common_name')
        fcode = request.form.get('fcode')
        if common_name == '' or fcode == '':
            return render_template('add_facility.html', status="Error: Facility must have common_name and fcode", items = items)
        elif len(common_name) > 32:
            return render_template('add_facility.html', status="Error: Facility name has a max length of 32 characters", items = items)
        elif len(fcode) > 6:
            return render_template('add_facility.html', status="Error: Fcode has a max length of 6 character", items = items)
        else:#Check if common_name or fcode are already taken
            cur.execute('SELECT common_name FROM facilities WHERE common_name = %s', (common_name,))
            check_name = cur.fetchone()
            if check_name == None:
                cur.execute('SELECT fcode FROM facilities WHERE fcode = %s', (fcode,))
                check_fcode = cur.fetchone()
                if check_fcode == None:
                    cur.execute('INSERT INTO facilities (common_name, fcode) VALUES (%s, %s)', (common_name, fcode))
                    cur.execute('SELECT common_name, fcode FROM facilities')
                    items = cur.fetchall()
                    return render_template('add_facility.html', status="New facility added to LOST", items = items)
                else:
                    return render_template('add_facility.html', status="Facility code taken", items = items)
            else:
                return render_template('add_facility.html', status="Facility name taken", items = items)


@app.route('/add_asset', methods=['GET', 'POST'])
def add_asset():
    cur.execute('SELECT fcode FROM facilities')
    facilities = cur.fetchall()
    cur.execute('SELECT a.asset_tag, a.description, f.fcode, a.aquired FROM assets a JOIN facilities f ON a.facility = f.fcode WHERE a.disposed IS NULL')
    items = cur.fetchall()
    if request.method == 'GET':
        return render_template('add_asset.html', facilities = facilities, items = items)
    if request.method == 'POST':
        asset_tag = request.form.get('asset_tag')
        print(asset_tag)
        description = request.form.get('description')
        print(description)
        date = request.form.get('date')
        print(date)
        facility = request.form.get('facility_list')
        print(facility)
        if asset_tag == '':
            return render_template('add_asset.html', facilities = facilities, status="Error: Must include an asset tag", items = items)
        elif facility =='':
            return render_template('add_asset.html', facilities = facilities, status="Error: Must include a facility")
        elif date =='':
            return render_template('add_asset.html', facilities = facilities, status="Error: Must include an arrival date", items = items)
        else: #Check for if asset tag is already in database
            cur.execute('SELECT asset_tag FROM assets WHERE asset_tag = %s', (asset_tag,))
            check_tag = cur.fetchone()
            if check_tag == None:#asset_tag not currently in database
                cur.execute('INSERT INTO assets (asset_tag, description, aquired, facility) VALUES (%s, %s, %s, %s)', (asset_tag, description, date, facility))
                cur.execute('SELECT a.asset_tag, a.description, f.fcode, a.aquired FROM assets a JOIN facilities f ON a.facility = f.fcode WHERE a.disposed IS NULL')
                items = cur.fetchall()
                return render_template('add_asset.html', facilities = facilities, status="Successfully inserted asset into database", items = items)
            else:#asset_tag in database
                return render_template('add_asset.html', facilities = facilities, status="Error: Asset Tag is taken", items = items)

@app.route('/dispose_asset', methods = ['GET', 'POST'])
def dispose_asset():
    if session.get('user') is None:#If no user signed in, redirect to login page
        return render_template('index.html')

    cur.execute('SELECT role FROM users WHERE username = %s', (session['user'],))
    check_role = cur.fetchone()
    print(check_role)
    if check_role[0] != "Logistics Officer":#User doesnt have access
        return render_template('dashboard.html')
    if check_role[0] == "Logistics Officer":#User has access
        cur.execute('SELECT a.asset_tag, a.description, f.fcode, a.aquired FROM assets a JOIN facilities f on a.facility= f.fcode WHERE a.disposed IS NULL')
        items = cur.fetchall()
        if request.method =='GET':
            return render_template('dispose_asset.html', items = items)
        if request.method =='POST':
            asset_tag = request.form.get('asset_tag')
            date = request.form.get('date')
            if asset_tag == '':
                return render_template('dispose_asset.html', status="Error: Must include an asset_tag", items = items)
            elif date == '':
                return render_template('dispose_asset.html', status="Error: Must include a disposed date", items = items)
            else:#Check if asset tag is in database
                cur.execute('SELECT asset_tag FROM assets WHERE asset_tag = %s', (asset_tag,))
                check_tag = cur.fetchone()
                if check_tag != None: #asset_tag currently in database
                    cur.execute('UPDATE assets SET disposed = %s WHERE asset_tag = %s', (date, asset_tag))
                    cur.execute('SELECT a.asset_tag, a.description, f.fcode, a.aquired FROM assets a JOIN facilities f on a.facility = f.fcode WHERE a.disposed IS NULL')
                    items = cur.fetchall()
                    return render_template('dispose_asset.html', status="Successfully marked asset as disposed", items = items)
                else: #asset_tag not in database
                    return render_template('dispose_asset.html', status="Asset Tag not in database", items = items)


@app.route('/asset_report', methods=['GET', 'POST'])
def asset_report():
    cur.execute('SELECT fcode FROM facilities')
    facilities = cur.fetchall()
    if request.method == 'GET':
        return render_template('asset_report.html', facilities = facilities)
    elif request.method == 'POST':
        asset_tag = request.form.get('asset_tag')
        date = request.form.get('date')
        fcode = request.form.get('facility_list')
        if asset_tag == '' or date == '':#If user did not enter asset tag or date
            return render_template('asset_report.html', facilities = facilities, status = "Must enter an asset tag and an arrival date")
        elif fcode == "All":#asset_tag and date not empty, fcode not specified
            cur.execute('SELECT a.asset_tag, a.arrival_date, f.fcode FROM assets a JOIN facilities f ON f.facility_pk = a.facility_fk WHERE (a.asset_tag, a.arrival_date) = (%s, %s)', (asset_tag, date))
            items = cur.fetchall()
            return render_template('asset_report.html', facilities = facilities, items = items)
        else: #asset_tag and date not empty, and fcode was specified
            cur.execute('SELECT a.asset_tag, a.arrival_date, f.fcode FROM assets a JOIN facilities f on f.facility_pk = a.facility_fk WHERE (a.asset_tag, a.arrival_date, f.fcode) = (%s, %s, %s)', (asset_tag, date, fcode))
            items = cur.fetchall()
            return render_template('asset_report.html', facilities = facilities, items = items)

@app.route('/transit_req', methods=['GET', 'POST'])
def transit_req():
    if session['user'] == None:
        return render_template('index.html', status = "Must be signed into LOST for access")
    elif session['role'] != "Logistics Officer":#Checks user role, if not correct role redirects to dashboard
        return render_template('dashboard.html', status = "Must be Logistics Officer to initiate transfer requests")
    else:#User has access        
        cur.execute('SELECT fcode FROM facilities')
        facilities = cur.fetchall()
        cur.execute('SELECT a.asset_tag, a.description, f.fcode, a.aquired FROM assets a JOIN facilities f on a.facility = f.fcode WHERE a.disposed IS NULL')
        items = cur.fetchall()
        if request.method == 'GET':
            return render_template('transit_req.html', facilities = facilities, items = items)
        elif request.method == 'POST':
            asset_tag = request.form.get('asset_tag')
            dest_fcode = request.form.get('facility_list')
            if asset_tag == '':
                return render_template('transit_req.html', facilities = facilities, status = "Must enter an asset tag for transit request", items = items)
            else: #Check if asset_tag is valid
                cur.execute('SELECT f.fcode FROM facilities f JOIN assets a ON f.fcode = a.facility WHERE a.asset_tag = %s', (asset_tag,))
                src = cur.fetchone()
                if src == None:
                    return render_template('transit_req.html', facilities = facilities, status = "This asset tag does not currently belong to an OSNAP facility", items = items)
                else:#asset_tag is valid
                    cur.execute('SELECT asset_tag FROM transit_req WHERE asset_tag = %s', (asset_tag,)) ##Check if this asset_tag already has a transit request
                    if src[0] == dest_fcode: #If the source fcode and dest fcode are same, dont create transit request
                        return render_template('transit_req.html', facilities = facilities, status = "Cannot complete a transfer request to the same facility", items = items)
                    check_req = cur.fetchone()
                    if check_req == None: #if asset does not have a transit request, make one
                        cur.execute('INSERT INTO transit_req (asset_tag, source, destination, request_by, request_dt, status) VALUES (%s,%s,%s,%s,now(),%s)', (asset_tag, src[0], dest_fcode, session['user'], "UNAPPROVED"))
                        return render_template('transit_req.html', facilities = facilities, status = "Successfully initiated transit request", items = items)
                    else: #If asset does have a transit request, do not make a new one.
                        return render_template('transit_req.html', facilities = facilities, status = "This asset already has a pending transit request", items = items)

@app.route('/approve_req', methods = ['GET', 'POST'])
def approve_req():
    if session['user'] == None:
        return render_template('index.html', status = "Must be signed into LOST for access")
    elif session['role'] != "Facilities Officer":
        return render_template('dashboard.html', status = "Must be a Facilities Officer to approve transit requests")
    else:#User has access
        if request.method == 'GET':
            current_tag = request.args['asset_tag']
            cur.execute('SELECT status FROM transit_req WHERE asset_tag = %s', (current_tag,))
            result = cur.fetchone()
            print(result[0])
            if result[0] == None or result[0] == "APPROVED":#Ensure that given asset tag is valid for approval
                return render_template('dashboard.html', status = "Invalid request")
            elif result[0] == "UNAPPROVED": #Send user to approval screen
                cur.execute('SELECT asset_tag, source, destination FROM transit_req WHERE asset_tag = %s', (current_tag,))
                requests = cur.fetchall()
                session['current_tag'] = current_tag
                return render_template('approve_req.html', requests = requests)
            #else:
        elif request.method == 'POST':
           # current_tag = request.args['asset_tag']
            approval_status = request.form.get("approval_result")
            if approval_status == "Approve":
                cur.execute('UPDATE transit_req SET status = %s, approve_by =%s, approve_dt = now() WHERE asset_tag = %s', ("APPROVED", session['user'], session['current_tag']))
                session['current_tag'] = None
                return redirect(url_for('dashboard'))
            elif approval_status == "Reject":
                cur.execute('DELETE FROM transit_req WHERE asset_tag = %s', (session['current_tag'],))
                session['current_tag'] = None
                return redirect(url_for('dashboard'))

@app.route('/update_transit', methods = ['GET', 'POST'])
def update_transit():
    if session['user'] == None:
        return render_template('index.html', status = "Must be signed into LOST for access")
    elif session['role'] != "Logistics Officer":
        return render_template('dashboard.html', status = "Must be a Logistics Officer to service approved transit requests.")
    else:#User has access
        if request.method == 'GET':
            current_tag = request.args['asset_tag']
            print(current_tag)
            cur.execute('SELECT asset_tag FROM transit_req WHERE asset_tag = %s AND status = %s', (current_tag, "APPROVED"))
            result = cur.fetchone()
            if result == None:
                return render_template('dashboard.html', status = "Invalid request")
            else:
                cur.execute('SELECT asset_tag, source, destination FROM transit_req WHERE asset_tag = %s', (current_tag,))
                requests = cur.fetchall()
                session['current_tag'] = current_tag
                return render_template('update_transit.html', requests = requests)
        elif request.method == 'POST':
            load_time = request.form.get("load_time")
            unload_time = request.form.get("unload_time")
            cur.execute('UPDATE transit_req SET load_dt = %s, unload_dt = %s, status = %s WHERE asset_tag = %s', (load_time, unload_time, "INTRANSIT", session['current_tag']))
            session['current_tag'] = None
            return redirect(url_for('dashboard'))
            
            
        #elif request.method == 'POST'

@app.route('/logout', methods=['GET'])        
def logout():
    session['user'] = None
    session['role'] = None
    return render_template('index.html', status = "Successfully Logged out")

@app.route('/dashboard', methods=['GET'])
def dashboard():
    if session['user'] == None:
        return render_template('index.html', status = "Must be signed into LOST for access")
    elif session['role'] == "Facilities Officer":
        cur.execute('SELECT * from transit_req WHERE status = %s', ("UNAPPROVED",))
        requests = cur.fetchall()
        return render_template('dashboard.html', requests = requests)
    elif session['role'] == "Logistics Officer":
        cur.execute('SELECT * FROM transit_req WHERE status = %s', ("APPROVED",))
        requests = cur.fetchall()
        return render_template('dashboard.html', requests = requests)






###This function checks if there is currently an active user
def check_activeuser():
    if session['user'] == None:
        return render_template('index.html', status = "Must be signed into LOST for accesss")
    else:
        return 1
###This function checks if 
def check_user_role(role):
    if session['role'] != role:
        return render_template('dashboard.html', status = "Invalid permission")
    else:
        return 1

###This function is used to generate this list of current facilties
def get_facilities():
    cur.execute('SELECT fcode FROM facilities')
    facilities = cur.fetchall
    return facilities


if __name__ =='__main__':
    app.run(host = '0.0.0.0', port = 8080)

