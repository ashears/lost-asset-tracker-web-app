#! /usr/bin/bash

# This script handles the setup that must occur prior to running LOST
# Specifically this script:
#    1. creates the database
#    2. copies the required source to $HOME/wsgi

if [ "$#" -ne 1 ]; then
    echo "Usage: ./prepreflight.sh <dbname>"
    exit;
fi
dropdb $1
initdb $1
pg_ctl -D $1 -l logfile start
createdb $1

