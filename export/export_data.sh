#!/bin/bash

#psql $1 -c /set v1 $2
#psql $1 -f export_data.sql
user=users.csv
facilities=facilities.csv
assets=assets.csv
transfers=transfers.csv
if [ ! -d $2 ]; then
	mkdir $2
fi

cd $2
#Export user table
psql $1 -c "\copy (SELECT u.username, u.password, u.active, r.role FROM users u JOIN roles r ON r.role_pk = u.role_fk) TO '"$user"' DELIMITER ',' CSV HEADER"
#Export facilities table
psql $1 -c "\copy (SELECT fcode, common_name FROM facilities) TO '"$facilities"' DELIMITER ',' CSV HEADER"
#Export assets table
psql $1 -c "\copy (SELECT a.asset_tag, a.description, f.fcode, a.arrival_date, a.dispose_date FROM assets a JOIN facilities f ON f.facility_pk = a.facility_fk) TO '"$assets"' DELIMITER ',' CSV HEADER"
#Export transfers table
psql $1 -c "\copy (SELECT asset_tag, request_by, request_dt, approve_by, approve_dt, source, destination, load_dt, unload_dt FROM transit_req) TO '"$transfers"' DELIMITER ',' CSV HEADER"
