#! /usr/bin/bash

# This script handles the setup that must occur prior to running LOST
# Specifically this script:
#    1. creates the database
#    2. copies the required source to $HOME/wsgi

if [ "$#" -ne 1 ]; then
    echo "Usage: ./preflight.sh <dbname>"
    exit;
fi

# Database prep
#createdb lost
cd sql
psql $1 -f create_tables.sql
cd ..
cd import
#curl -O https://classes.cs.uoregon.edu//17W/cis322/files/lost_data.tar.gz
#tar -xvzf lost_data.tar.gz
#rm lost_data.tar.gz
cd ..
# Install the wsgi files
cp -R src/* $HOME/wsgi


