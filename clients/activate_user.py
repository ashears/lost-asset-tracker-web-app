import sys
from urllib.request import Request, urlopen
from urllib.parse import urlencode
def main():
    #Check the CLI arguemtns
    if len(sys.argv)< 5:
        print("Usage: python3 %s <url> <username> <password> <role>"%sys.argv[0])
        return

    args = dict()
    args['username'] = sys.argv[2]
    args['password'] = sys.argv[3]
    if sys.argv[4] == "facofc":
        args['role'] = "Facilities Officer"
    elif sys.argv[4] == "logofc":
        args['role'] = "Logistics Officer"
    else:
        print("Invalid role. Valid entries are facofc for Facilities Officer and logofc for Logistics Officer")
        return

    data = urlencode(args)
    my_route = "/activate_user"
    req = Request(sys.argv[1] + my_route, data.encode('ascii'), method ='POST')
    res = urlopen(req)


if __name__=='__main__':
    main()
