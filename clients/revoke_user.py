import sys
from urllib.request import Request, urlopen
from urllib.parse import urlencode
def main():
    #Check the CLI arguemtns
    if len(sys.argv)< 3:
        print("Usage: python3 %s <url> <username>"%sys.argv[0])
        return

    args = dict()
    args['username'] = sys.argv[2]
    data = urlencode(args)
    my_route = "/revoke_user"
    req = Request(sys.argv[1] + my_route, data.encode('ascii'), method ='POST')
    res = urlopen(req)


if __name__=='__main__':
    main()
